import logging
import time

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


# Clase de la página del resultado seleccionado de BandCamp.
# author: Javier Adalid
class SelectedResultPage():
    driver = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' SELECTED RESULT PAGE')

    # Constructor que recibe como parámetro un WebDriver
    def __init__(self, driver: webdriver):
        self.driver = driver

    # Comienza a reproducir la selección
    def play(self):

        wait = WebDriverWait(self.driver, 10)
        btn=wait.until(ec.presence_of_element_located
                                     ((By.XPATH, "//div[@class='playbutton']")))
        btn.click()
        self.logger.info("Se hizo un click en reproducir.")
        time.sleep(10)
        return self.driver.execute_script("return document.querySelector('audio').paused")