import logging

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


# Clase de la página de inicio (búsqueda) de BandCamp
# author: Javier Adalid
class SearchPage():
    driver = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' BANDCAMP SEARCH ')

    # Constructor que recibe un Webdriver como parámetro
    def __init__(self, driver: webdriver):
        self.driver = driver

    # Obtiene y navega a la página principal de BandCamp
    def get_bandcamp_homepage(self):
        self.driver.get("https://bandcamp.com/")
        self.logger.info("Estoy en la homepage de BandCamp")

    # Realiza una búsqueda en BandCamp
    def bandcamp_search(self):
        wait = WebDriverWait(self.driver, 10)
        searchInput = wait.until(ec.visibility_of_element_located
                                 ((By.XPATH, "//input[contains(@placeholder,'and')]")))
        query = input("Ingresa tu búsqueda: ")
        searchInput.clear()
        searchInput.send_keys(str(query))
        searchInput.send_keys(Keys.ENTER)
        self.logger.info("Se realizó la búsqueda de: '" + query + "'")
