import logging
import string

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait


# Clase de la página de resultados de BandCamp
# author: Javier Adalid
class ResultsPage():
    driver = None
    results = None
    query = None
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' RESULTS PAGE')


    # Constructor que recibe como parámetro un WebDriver
    def __init__(self, driver: webdriver):
        self.driver = driver

    # Método que obtiene todos los resultados de la búsqueda
    def get_results(self):
        try:
            cont=0
            wait = WebDriverWait(self.driver, 10)
            self.results = wait.until(
                ec.presence_of_all_elements_located
                ((By.XPATH, "//div[@class='heading']//a")))
            for result in self.results:
                if (result.text):
                    cont+=1
                    print(str(cont) + ". " + result.text)
                    print("-------------------------------------------------")
        except NoSuchElementException:
            self.logger.info("No hubo ningún resultado de búsqueda")
            return


    def select_result(self):
        result = None
        ban = True
        while ban:
            try:
                res= int(input("¿Cuál de los resultados quieres reproducir? "
                           " Indica el número únicamente."))
                if(res <= len(self.results) and res>0):
                    ban=False
                    self.query = self.driver.find_element_by_xpath(
                        "//input[@id='q']"). \
                        get_attribute('value')
                    self.logger.info("Se seleccionó el resultado: "
                                     + self.results[res-1].text)
                    result = self.results[res - 1].text
                    self.results[res - 1].click()
                else:
                    print("Ingresa una opción válida")
            except ValueError:
                print("Ingresa sólo números")
        return result

    # Verifica que el resultado seleccionado contenga alguna de
    # las palabras proporcionadas en la búsqueda.
    def verify_result_title(self, seleccion):
        flag = False
        if(self.query != None):
            result = seleccion.lower()
            tildes, normal = 'áéíóúü', 'aeiouu'
            resTranslation = result.maketrans(tildes, normal)
            queryTranslation = self.query.maketrans(tildes, normal)
            tResult = result.translate(resTranslation)
            tQuery=self.query.translate(queryTranslation)
            if tQuery in tResult:
                flag = True
                self.logger.info("El resultado coincide con la búsqueda")
        return flag

