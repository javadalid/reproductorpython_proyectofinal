import logging
import unittest

from pom.ResultsPage import ResultsPage
from pom.SearchPage import SearchPage
from pom.SelectedResultPage import SelectedResultPage as SRP
from setup.Setup import Setup


# Clase Bandcamp test que utiliza a unittest para la
# ejecución del caso de prueba.
# author: Javier Adalid.
class Test_Bandcamp(unittest.TestCase):
    # Declaraciones necesarias para el test
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger(' MAIN TEST ')
    searchPage = None
    resultsPage = None
    driver = None
    result = None

    @classmethod
    def setUpClass(self) -> None:
        chromeDriver = Setup('ch')
        self.driver = chromeDriver.get_driver()
        self.searchPage = SearchPage(self.driver)
        self.resultsPage = ResultsPage(self.driver)
        self.selectedResultPage = SRP(self.driver)
        self.searchPage.get_bandcamp_homepage()
        self.searchPage.bandcamp_search()
        self.resultsPage.get_results()
        self.result = self.resultsPage.select_result()

    def test_result_titlee(self):
        self.assertTrue(self.resultsPage.verify_result_title(self.result),
                        "El resultado seleccionado NO contiene alguno"
                        " se los términos buscados.")

    def test_sound(self):
        self.assertFalse(self.selectedResultPage.play(),
                    "El audio NO se está reproduciendo")

    @classmethod
    def tearDownClass(self) -> None:
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
